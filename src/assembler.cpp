#include <bitset>
#include <cstddef>
#include <cstdio>
#include <cwchar>
#include <exception>
#include <filesystem>
#include <iterator>
#include <memory>
#include <stdexcept>
#include <string>

#include "../include/assembler.hpp"
#include "../include/lexer.hpp"
#include "../include/regular_expressions.hpp"

using namespace std;

Assembler::Assembler(const string &src, const string &dest, bool binary)
    : src_path{src}, dest_path{dest}, binary_mode(binary) {
    if (filesystem::is_empty(src_path) || !filesystem::exists(src_path)) {
        throw invalid_argument{"Invalid source file "s + '"' +
                               src_path.string() + '"' + " specified"s};
    } else if (src_path == dest_path) {
        throw invalid_argument{"Source file "s + '"' + src_path.string() + '"' +
                               " is the same as destination file"s};
    }

    lexer = make_unique<Lexer>();
    parser = make_unique<Parser>();

    source_file.open(src_path);

    auto dest_mode = ios::out;
    write_output = [this](const bitset<bits> &code) {
        dest_file << code.to_string() << '\n';
    };

    if (binary_mode) {
        dest_mode |= ios::binary;
        write_output = [this](const bitset<bits> code) {
            dest_file.put(code.to_ulong());
        };
    }

    dest_file.open(dest_path, dest_mode);

    /* TODO: need to find a better way of handling comp bits */
    comp_bits = {
        {"0", "0101010"},   {"1", "0111111"},   {"-1", "0111010"},
        {"D", "0001100"},   {"A", "0110000"},   {"M", "1110000"},
        {"!D", "0001101"},  {"!A", "0110001"},  {"!M", "1110001"},
        {"-D", "0001111"},  {"-A", "0110011"},  {"-M", "1110011"},
        {"D+1", "0011111"}, {"A+1", "0110111"}, {"M+1", "1110111"},
        {"D-1", "0001110"}, {"A-1", "0110010"}, {"M-1", "1110010"},
        {"D+A", "0000010"}, {"D+M", "1000010"}, {"D-A", "0010011"},
        {"D-M", "1010011"}, {"A-D", "0000111"}, {"M-D", "1000111"},
        {"D&A", "0000000"}, {"D&M", "1000000"}, {"D|A", "0010101"},
        {"D|M", "1010101"},
    };
}

Assembler::~Assembler(void) {
    source_file.close();
    dest_file.close();
}

void Assembler::assemble() {
    run_first_pass();
    run_second_pass();
}

void Assembler::run_first_pass(void) {
    size_t line_idx = 0;
    /* clear the eof bits so that getline() works again */
    source_file.clear();
    source_file.seekg(0);
    for (string line; getline(source_file, line);) {
        auto line_type = lexer->tokenize_line(line);
        if (line_type == Token::Label) {
            parser->parse_label(line, line_idx);
            /* don't count label as a line too */
            continue;
        } else if (line_type == Token::Comment) {
            /* ignore comments */
            continue;
        } else if (line_type == Token::Invalid) {
            throw invalid_argument{"Assembler::run_first_pass(): "s +
                                   "Invalid statement -> "s + '"' + line + '"'};
        }
        line_idx++;
    } // end for
}

void Assembler::run_second_pass(void) {
    source_file.clear();
    source_file.seekg(0);
    for (string line; getline(source_file, line);) {
        auto line_type = lexer->tokenize_line(line);
        bitset<bits> code{};
        if (line_type == Token::A_Instruction) {
            parser->parse_a_instruction(line);
            code = handle_a_instruction(line);
        } else if (line_type == Token::C_Instruction) {
            code = handle_c_instruction(line);
        } else {
            continue;
        }
        write_output(code);
    }
}

bitset<bits> Assembler::handle_a_instruction(const string &line) {
    /* retrieve the value of the A instruction */
    smatch match{};
    regex_search(line, match, a_instruction_rx);
    string operand = match[1].str();

    size_t value = parser->get_symbol(operand);

    /* check if the first letter is a digit */
    bitset<bits> code{};

    code.reset();

    /* add the value to the instruction */
    code |= value;

    return code;
}

bitset<bits> Assembler::handle_c_instruction(const string &line) {
    /* retrieve the value of the A instruction */
    smatch matches{};
    regex_search(line, matches, c_instruction_rx);

    /* retrieve the dest, comp and jump fields */

    /* dest = matches[2], comp = matches[3], jump = matches[5] */
    string dest = (matches[2].matched) ? matches[2].str() : "";
    string comp = (matches[3].matched) ? matches[3].str() : "";
    string jump = (matches[5].matched) ? matches[5].str() : "";

    /* bits:  0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
     * field: 1 1 1 a c c c c c c d  d  d  j  j  j
     */
    bitset<bits> code{"111" + handle_comp(comp) + handle_dest(dest) +
                      handle_jump(jump)};

    return code;
}

string Assembler::handle_dest(const string &dest) const {
    bitset<3> dest_bits;

    dest_bits.reset();

    if (dest.find('M') != string::npos) {
        dest_bits.set(0);
    }
    if (dest.find('D') != string::npos) {
        dest_bits.set(1);
    }
    if (dest.find('A', 0) != string::npos) {
        dest_bits.set(2);
    }

    return dest_bits.to_string();
}

string Assembler::handle_comp(const string &comp) const {
    using namespace std;
    using namespace string_literals;

    auto found = comp_bits.find(comp);
    if (found == comp_bits.end()) {
        throw runtime_error{"Error handling comp field of C instruction -> "s +
                            comp};
    }
    return found->second;
}

string Assembler::handle_jump(const string &jump) const {
    bitset<3> jump_bits{};

    jump_bits.reset();

    if (jump == "") {
        return jump_bits.to_string();
    }

    auto jmp_greater_than = [&jump_bits]() { jump_bits.set(0); };
    auto jmp_equal = [&jump_bits]() { jump_bits.set(1); };
    auto jmp_less_than = [&jump_bits]() { jump_bits.set(2); };

    if (jump == "JGT") {
        jmp_greater_than();
    } else if (jump == "JEQ") {
        jmp_equal();
    } else if (jump == "JLT") {
        jmp_less_than();
    } else if (jump == "JGE") {
        jmp_greater_than();
        jmp_equal();
    } else if (jump == "JLE") {
        jmp_less_than();
        jmp_equal();
    } else if (jump == "JNE") {
        jmp_less_than();
        jmp_greater_than();
    } else if (jump == "JMP") {
        jmp_less_than();
        jmp_greater_than();
        jmp_equal();
    } else {
        throw runtime_error{"Error handling jump field of C instruction -> "s +
                            jump};
    }

    return jump_bits.to_string();
}
