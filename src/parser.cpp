#include <iostream>
#include <regex>
#include <stdexcept>
#include <string>

#include "../include/parser.hpp"
#include "../include/regular_expressions.hpp"

using namespace std;
using namespace std::string_literals;

Parser::Parser(void) : symbols{}, variable_address{16} {
    /* populate the symbol table with some pre-defined symbols */

    symbols["SP"s] = 0;
    symbols["LCL"s] = 1;
    symbols["ARG"s] = 2;
    symbols["THIS"s] = 3;
    symbols["THAT"s] = 4;

    /* R0 - R15 */
    for (int i = 0; i < 16; ++i) {
        symbols["R"s + to_string(i)] = i;
    }

    /* other symbols will go between 16-0x4000 */

    symbols["SCREEN"s] = 0x4000;
    symbols["KBD"s] = 0x6000;
}

void Parser::parse_label(const std::string &line, size_t line_idx) {
    /* retrieve the captured label in regex expression,
     * and store it in the table
     */
    smatch match;
    regex_search(line, match, label_rx);

    if (!match[1].matched) {
        throw runtime_error{
            "Parses::parse_label(): Couldn't parse the given label -> "s + '"' +
            line + '"'};
    }
    symbols[match[1].str()] = line_idx;
}

void Parser::parse_a_instruction(const std::string &line) {
    smatch match;
    regex_search(line, match, a_instruction_rx);

    if (!match[1].matched) {
        throw runtime_error{
            "Parser::parse_a_instruction(): Couldn't parse A instruction -> "s +
            '"' + line + '"'};
    }

    auto symbol = match[1].str();

    /* check if it's a digit */
    if (regex_match(symbol, regex(R"(^\d+$)"))) {
        symbols[symbol] = stoi(symbol);
    } else if (symbols.find(symbol) == symbols.end()) {
        /* if there's a symbol already then
         * we must have dealt it as a label
         * so don't overwrite its value
         */
        symbols[symbol] = variable_address++;
    }
}

size_t Parser::get_symbol(const std::string &symbol) {
    return symbols.at(symbol);
}
