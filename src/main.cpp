#include <cstdlib>
#include <fmt/color.h>
#include <getopt.h>
#include <iomanip>
#include <ios>
#include <iostream>
#include <memory>
#include <string>

#include "../include/assembler.hpp"
#include "../include/exception.hpp"

using namespace std;
using namespace std::string_literals;

using fmt::print;
using fmt::terminal_color;

static void show_usage(std::string, bool = true);
static tuple<string, string, bool> cook_args(int, char **);

int main(int argc, char *argv[]) {
    try {
        /* This helps avoid some overhead that comes from having to sync with
         * the C-style I/O facilities. This way, the C++ streams can handle
         * their own buffering rather than immediately writing to the C streams.
         * However, Valgrind reports a memory leak when this is put but with
         * some digging around, I think it isn't something to worry about.
         * Source:
         * https://news.ycombinator.com/item?id=7055040
         */
        ios_base::sync_with_stdio(false);

        auto [source_file, output_file, binary] = cook_args(argc, argv);
        auto assembler =
            make_unique<Assembler>(source_file, output_file, binary);

        assembler->assemble();

        /* if we use exit() here, then the destructor for Assembler won't be
         * called. */
        return 0;
    } catch (...) {
        process_exception();
        exit(EXIT_FAILURE);
    }
}

void show_usage(std::string name, bool banner) {
    if (banner) {
        print(fg(terminal_color::bright_white),
              "An assembler for the Hack machine language.\n\n");
    }
    print(fg(terminal_color::bright_yellow), "Usage: ");
    print(fg(terminal_color::bright_white), "{} [OPTIONS] SOURCE\n\n", name);
    print(fg(terminal_color::bright_yellow), "OPTIONS:\n");
    print(fg(terminal_color::bright_green), "\t-h\t\t");
    print(fg(terminal_color::bright_white), "Show this help message\n");
    print(fg(terminal_color::bright_green), "\t-o PATH\t\t");
    print(fg(terminal_color::bright_white),
          "The output file path; default is \"a.hack\"\n");
    print(fg(terminal_color::bright_green), "\t-b\t\t");
    print(fg(terminal_color::bright_white), "Write output in binary mode\n");
    print(fg(terminal_color::bright_yellow), "ARGS:\n");
    print(fg(terminal_color::bright_green), "\t<SOURCE>\t");
    print(fg(terminal_color::bright_white), "Source file path\n");
}

tuple<string, string, bool> cook_args(int argc, char **argv) {
    string output_file{"a.hack"};
    bool binary = false;

    int opt;
    while ((opt = getopt(argc, argv, "bho:")) != -1) {
        switch (opt) {
        case 'h':
            show_usage(argv[0]);
            exit(EXIT_SUCCESS);
            break;
        case 'o':
            output_file = optarg;
            break;
        case 'b':
            binary = true;
            break;
        default:
            print(fg(terminal_color::bright_red), "ERROR: ");
            print(fg(terminal_color::bright_white),
                  "Invalid arguments given\n\n");
            show_usage(argv[0], false);
            exit(EXIT_FAILURE);
            break;
        }
    }

    /* if option index is already at the end of the given arguments,
     * then we'll know that SOURCE wasn't given */
    if (optind >= argc) {
        print(fg(terminal_color::bright_red), "ERROR: ");
        print(fg(terminal_color::bright_white), "Source file not given\n\n");
        show_usage(argv[0], false);
        exit(EXIT_FAILURE);
    }

    return {argv[optind], output_file, binary};
}
