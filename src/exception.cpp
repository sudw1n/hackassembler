#include <exception>
#include <fmt/color.h>
#include <future>
#include <ios>
#include <iostream>
#include <stdexcept>
#include <system_error>

#include "../include/exception.hpp"

void process_exception(void) noexcept {
    using namespace std;
    using namespace std::string_literals;
    try {
        throw; // rethrow exception to deal here
    } catch (const ios_base::failure &e) {
        fmt::print(fmt::fg(fmt::terminal_color::bright_red), "I/O EXCEPTION: ");
        fmt::print(fmt::fg(fmt::terminal_color::bright_white), "{}\n",
                   e.what());
    } catch (const system_error &e) {
        fmt::print(fmt::fg(fmt::terminal_color::bright_red),
                   "SYSTEM  EXCEPTION: ");
        fmt::print(fmt::fg(fmt::terminal_color::bright_white), "{}\n",
                   e.what());
    } catch (const invalid_argument &e) {
        fmt::print(fmt::fg(fmt::terminal_color::bright_red),
                   "INVALID ARGUMENT: ");
        fmt::print(fmt::fg(fmt::terminal_color::bright_white), "{}\n",
                   e.what());
    } catch (const exception &e) {
        fmt::print(fmt::fg(fmt::terminal_color::bright_red), "EXCEPTION: ");
        fmt::print(fmt::fg(fmt::terminal_color::bright_white), "{}\n",
                   e.what());
    } catch (...) {
        fmt::print(fmt::fg(fmt::terminal_color::bright_red), "EXCEPTION: ");
        fmt::print(fmt::fg(fmt::terminal_color::bright_white),
                   "Unknown exception occurred\n");
    }
}
