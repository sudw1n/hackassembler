#include <regex>
#include <string>

#include "../include/lexer.hpp"
#include "../include/regular_expressions.hpp"

Lexer::Lexer(void) : tokenized_statements{} {}

Token Lexer::tokenize_line(const std::string &line) {
    /* check if said statement has already been tokenized */
    auto found = tokenized_statements.find(line);
    if (found != tokenized_statements.end()) {
        return found->second;
    }

    /* default is invalid */
    Token line_type = Token::Invalid;

    if (is_comment(line)) {
        line_type = Token::Comment;
    } else if (is_label(line)) {
        line_type = Token::Label;
    } else if (is_a_instruction(line)) {
        line_type = Token::A_Instruction;
    } else if (is_c_instruction(line)) {
        line_type = Token::C_Instruction;
    }

    /* using emplace() so that the passed object need not be copied over */
    tokenized_statements.emplace(line, line_type);
    return line_type;
}

bool Lexer::is_comment(const std::string &line) const {
    return std::regex_match(line, comment_rx);
}

bool Lexer::is_a_instruction(const std::string &line) const {
    return std::regex_match(line, a_instruction_rx);
}

bool Lexer::is_c_instruction(const std::string &line) const {
    return std::regex_match(line, c_instruction_rx);
}

bool Lexer::is_label(const std::string &line) const {
    return std::regex_match(line, label_rx);
}
