# HackAssembler

### Introduction
Hack is a machine language built by the creators of [nand2tetris](https://nand2tetris.org). This
assembler assembles the source files written in Hack assembly into a text file where each line is an
instruction in binary (string form rather than raw binary). A snippet of the Hack machine language
specification is as follows:

![Instruction specification](./Instruction.png)

### How to Compile
**Requirements**:
- [CMake](https://cmake.org)
- [{fmt}](https://fmt.dev)
- [clang++](https://clang.llvm.org/) or [g++](https://gcc.gnu.org/)

Clone the repo:

```
git clone -b 'main' --depth 1 'https://gitlab.com/sudw1n/hackassembler.git/'
```

**Note**: If you don't want to use the [{fmt}](https://fmt.dev) library, then clone the
[basic](https://gitlab.com/sudw1n/hackassembler/-/tree/basic) branch containing a more basic version
of the same program:

```
git clone -b 'basic' --depth 1 'https://gitlab.com/sudw1n/hackassembler.git/'
```

Then go to the project directory and run the following command:

```
cmake -B build/
```

This will generate the necessary (Makefiles) files required for building our program. This needs
only be run once (unless you delete the `build/` directory). Now run:

```
cmake --build build/ -j3
```

After that's done, the final executable will be stored at `build/assembler`.
Give the `-h` flag for help:

```
$ build/assembler -h
An assembler for the Hack machine language.

Usage: build/assembler [OPTIONS] SOURCE

OPTIONS:
        -h              Show this help message
        -o PATH         The output file path; default is "a.hack"
        -b              Write output in binary mode
ARGS:
        <SOURCE>        Source file path
```

This should be self-explanatory.
