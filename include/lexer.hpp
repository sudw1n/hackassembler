#pragma once
#include <regex>
#include <string>
#include <unordered_map>

enum class Token { Comment, A_Instruction, C_Instruction, Label, Invalid };

class Lexer {
  public:
    Lexer(void);
    /* determine the type of the line */
    Token tokenize_line(const std::string &);

  private:
    /* for storing already handled statements */
    std::unordered_map<std::string, Token> tokenized_statements;

    bool is_comment(const std::string &) const;
    bool is_a_instruction(const std::string &) const;
    bool is_c_instruction(const std::string &) const;
    bool is_label(const std::string &) const;
};
