#pragma once
#include <filesystem>
#include <fstream>
#include <functional>
#include <memory>
#include <string>
#include <unordered_map>

#include "lexer.hpp"
#include "parser.hpp"

class Assembler {
  public:
    Assembler(const std::string &src_path, const std::string &dest_path, bool);
    ~Assembler(void);
    void assemble(void);

  private:
    /* file system paths with extension */
    const std::filesystem::path src_path;
    const std::filesystem::path dest_path;

    /* for determining the different types of statements */
    std::unique_ptr<Lexer> lexer;
    /* for handling symbols */
    std::unique_ptr<Parser> parser;

    /* file containing assembly program */
    std::ifstream source_file;
    /* where to write the final output,
     * i.e., after second pass
     */
    std::ofstream dest_file;

    /* whether or not we want the output to be written in binary mode */
    bool binary_mode;

    /* only parse labels */
    void run_first_pass(void);
    /* parse variables, and generate code */
    void run_second_pass(void);

    /* writes output depending on the binary_mode */
    std::function<void(const std::bitset<bits> &)> write_output;

    std::bitset<bits> handle_a_instruction(const std::string &line);

    std::bitset<bits> handle_c_instruction(const std::string &line);

    std::string handle_dest(const std::string &dest) const;
    std::unordered_map<std::string, std::string> comp_bits;
    std::string handle_comp(const std::string &comp) const;
    std::string handle_jump(const std::string &jump) const;
};
