#pragma once
#include <bitset>
#include <string>
#include <unordered_map>

constexpr const auto bits = 16;

class Parser {
  public:
    Parser(void);
    /* for first pass */
    void parse_label(const std::string &line, size_t line_idx);
    void parse_a_instruction(const std::string &line);
    size_t get_symbol(const std::string &symbol);

  private:
    /* for handling labels and variables */
    std::unordered_map<std::string, size_t> symbols;
    size_t variable_address;
};
