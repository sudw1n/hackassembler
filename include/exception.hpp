#pragma once
#include <iostream>

/* exception handler for every kind of exception */
void process_exception(void) noexcept;
