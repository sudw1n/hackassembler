#pragma once
#include <regex>
#include <string>

/* This file just contains the regular expressions for matching against
 * statements */

using namespace std::string_literals;
using std::regex;

/* starting with any number of white-space characters,
 * followed by a '//',
 * followed by any number of characters.
 * other statements may also contain comment after them
 */
const auto comment_rx = regex(R"((?:\s*//.*)?$)"s);

/* starting with any number of white-space characters,
 * followed by an '@',
 * followed by at least one character
 * (we keep it in a capture group to use later),
 * followed by any number of white-space characters.
 */
const auto a_instruction_rx = regex(R"(^\s*@([\w\.0-9\$]+)\s*(?:\s*//.*)?$)"s);

/* starting with any number of white-space characters,
 * followed by an opening '(',
 * followed by at least one character
 * (we keep it in a capture group to use later),
 * followed by a closing ')'
 * followed by any number of white-space character.
 */
const auto label_rx = regex(R"(^\s*\(([\w\.\$]+)\)\s*(?:\s*//.*)?$)"s);

/* This expression captures three things: dest, comp and jump.
 *
 * The form of statement will be:
 * dest = comp; jump
 *
 * dest and jump are optional, comp is mandatory.
 * if dest is present, the '=' is mandatory.
 * if jump is present, the ';' is mandatory.
 *
 * Break-down of regex:
 *
 * `((\w{1,3})\s*=\s*)?`: Matches the "dest" field, which consists of one to
 * three word characters, followed by optional white-space, followed by an
 * equals sign, followed by optional white-space. This entire "dest" field is
 * enclosed in a capture group so that it can be accessed later using the
 * corresponding capture group (e.g. matches[2]). The entire group is made
 * optional by the use of the "?" quantifier.
 *
 *  `([\w+\-|\&!]{1,3})`: Matches the mandatory "comp" field, which consists of
 * one to three word characters, plus any of the characters '+', '-', '&', '!',
 * or '|'. This entire "comp" field is enclosed in a capture group so that it
 * can be accessed later using the corresponding capture group (e.g.
 * matches[3]).
 *
 *  `(\s*;\s*(\w{1,3}))?`: Matches the "jump" field, which consists of a
 * semicolon character, optionally surrounded by white-space, followed by one to
 * three word characters. The entire "jump" field is enclosed in a capture group
 * so that it can be accessed later using the corresponding capture group (e.g.
 * matches[5]). The entire group is made optional by the use of the "?"
 * quantifier.
 */
const auto c_instruction_rx = regex(
    R"(^\s*((\w{1,3})\s*=\s*)?([\w+\-|\&!]{1,3})(\s*;\s*(\w{1,3}))?\s*(?:\s*//.*)?$)"s);
