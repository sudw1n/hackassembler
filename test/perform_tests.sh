#!/bin/sh

echo "Ensuring that the latest version of Assembler is compiled";
cmake --build ../build -j3

for path in $(echo asm/*); do
    fullname=$(basename -- "$path");
    filename="${fullname%.*}";

    destpath="out/$filename.out";
    hackpath="hack/$filename.hack";

    echo "Assembling $path to $destpath";
    ../build/assembler "$path" -o "$destpath" || exit 2;
    echo "Successfully assembled $filename";

    echo "Comparing $destpath with $hackpath";

    if ! diff -q $destpath $hackpath >/dev/null 2>&1; then
        echo -ne "\n!! TEST FAILED FOR $filename !!\n";
        echo -ne "\nExiting...\n";
        exit 1;
    fi

    echo "Test succeeded for $filename";
    echo -ne "\n\n";
done

echo -ne "!!! TEST SUCCEEDED !!!\n\n";
